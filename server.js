var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
var path = require('path');
var movimientosv2JSON = require('./movimientosv2.json');
var bodyParser = require('body-parser');
app.use(bodyParser.json())
app.listen(port);
console.log('todo list RESTful API server started on: ' + port);
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname,'index.html'));
}) //peticion get usando la url raiz
app.post('/', function(req, res){
  rest.send('hemos recibido su peticion');
})
app.put('/', function(req, res){
  rest.send('hemos recibido su peticion put');
})
app.delete('/', function(req, res){
  rest.send('hemos recibido su peticion delete cambiada');
})
app.get('/Clientes/:idcliente', function(req, res) {
  res.send('Aqui tiene al cliente numero: ' + req.params.idcliente);
})
app.get('/v1/Movimientos', function(req, res) {
  res.sendFile('movimientosv1.json');
})
app.get('/v2/Movimientos', function(req, res) {
  res.json(movimientosv2JSON);
})
app.get('/v2/Movimientos/:id', function(req, res) {
  console.log(req.params.id);
  res.send(movimientosv2JSON[req.params.id]);
})
app.get('/v2/Movimientosquery', function(req, res){ //para enviar mas registors se usa query
  console.log(req.query);
  res.send('recibido:' + req.query.nombre);
})
app.post('/v2/Movimientos', function (req, res){
  var nuevo = req.body
  nuevo.id = movimientosv2JSON.length + 1
  movimientosv2JSON.push(nuevo)
  resp.send('Movimiento dado de alta con id:' + nuevo.id)
})
